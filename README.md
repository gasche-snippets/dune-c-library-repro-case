Reproduction repository for a Dune issue:
  [ocaml/dune#4409: confused by Dune foreign_libraries doc: how to define a C library used by C stubs?](https://github.com/ocaml/dune/issues/4409)
