#include <caml/mlvalues.h>
#include "../lib-foo/foo.h"

value call_foo(value unit) {
  return Val_long(foo());
}
